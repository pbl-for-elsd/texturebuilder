package visitor;

import gen.langBaseVisitor;
import gen.langParser;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.List;

public class WorkVisitor extends langBaseVisitor<ProgramContext> {
    private final ProgramContext context = new ProgramContext();

    @Override
    public ProgramContext visitSource_code(langParser.Source_codeContext ctx) {
        for (ParseTree node : ctx.children) {
            if (node.getClass().equals(langParser.DefinitionContext.class))
                visitDefinition((langParser.DefinitionContext) node);
            else if (node.getClass().equals(langParser.Function_callContext.class))
                visitFunction_call((langParser.Function_callContext) node);
        }
        return context;
    }

    @Override
    public ProgramContext visitDefinition(langParser.DefinitionContext ctx) {
        String varName = ctx.var_name().CHAR().getText();

        if (ctx.literal() != null) {
            visitLiteral(ctx.literal());
        } else {
            visitFunction_call(ctx.function_call());
        }

        return context;
    }

    @Override
    public ProgramContext visitFunction_call(langParser.Function_callContext ctx) {
        String callVarName = null;
        if (ctx.var_name() != null) {
            callVarName = ctx.var_name().getText();

            if (!context.isVariableDefined(callVarName)) {
                // TODO: throw UndefinedVariableException
            }
        }

        String funName = ctx.FUN_NAME().getText();
        var funManager = new FunctionContextManager(callVarName, funName);
        context.functionManagers.push(funManager);

        List<langParser.ParameterContext> params = ctx.parameter();
        for (var param: params) { visitParameter(param); }
        return context;
    }

    @Override
    public ProgramContext visitParameter(langParser.ParameterContext ctx) {
        FunctionContextManager funManager = context.functionManagers.peek();

        var param = new FunctionContextManager.Parameter();
        if (ctx.literal() != null) {
            visitLiteral(ctx.literal());
            param.literal = context.currentLiteral;
            context.currentLiteral = null;
        } else if (ctx.var_name() != null) {
            visitVar_name(ctx.var_name());
        } else {
            visitFunction_call(ctx.function_call());
        }

        return context;
    }

    @Override
    public ProgramContext visitLiteral(langParser.LiteralContext ctx) {
        ProgramContext.Literal lit = new ProgramContext.Literal();
        if (ctx.INT() != null) {
            lit.intValue = Integer.parseInt(ctx.INT().getText());
        } else {
            lit.stringValue = ctx.STRING().getText();
        }

        context.currentLiteral = lit;
        return context;
    }

    @Override
    public ProgramContext visitVar_name(langParser.Var_nameContext ctx) {
        context.currentVariable = ctx.CHAR().getText();

        return context;
    }

}
