package visitor;

import java.util.HashMap;
import java.util.Stack;

public class ProgramContext {
    Stack<FunctionContextManager> functionManagers;
    HashMap<String, Object> variables;

    public Literal currentLiteral;
    public String currentVariable;

    public ProgramContext() {
        functionManagers = new Stack<>();
        variables = new HashMap<>();
    }

    public static class Literal {
        String stringValue;
        Integer intValue;
    }

    public boolean isVariableDefined(String varName) {
        return variables.containsKey(varName);
    }

}
