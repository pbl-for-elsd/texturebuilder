package visitor;

import java.util.ArrayList;
import java.util.List;

public class FunctionContextManager {
    public String callVarName;
    public String funName;
    public List<Parameter> parameters;

    public FunctionContextManager(String callVarName, String funName) {
        this.callVarName = callVarName;
        this.funName = funName;
        parameters = new ArrayList<>();
    }


    public static class Parameter {
        String varName;
        ProgramContext.Literal literal;
        FunctionContextManager funCall;
    }

}
