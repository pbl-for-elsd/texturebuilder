
import gen.langBaseListener;
import gen.langBaseVisitor;
import gen.langLexer;
import gen.langParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import visitor.WorkVisitor;

import java.io.IOException;


public class Main {

    public static void main(String[] args) throws IOException {
        langLexer lexer = new langLexer(CharStreams.fromFileName("src/test.txt"));
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        langParser parser = new langParser(tokens);

        langParser.Source_codeContext src_context = parser.source_code();
        WorkVisitor visitor = new WorkVisitor();
        visitor.visitSource_code(src_context);
    }

    public static void traverseTreeUsingListener(ParseTree ctx) {
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(new langBaseListener(), ctx);
    }

    public static void traverseTreeUsingVisitor(langParser.Source_codeContext ctx) {
        langBaseVisitor visitor = new langBaseVisitor();
        visitor.visitSource_code(ctx);
    }

}
